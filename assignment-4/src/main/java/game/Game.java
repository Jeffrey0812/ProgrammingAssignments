import board.Board;
import card.Card;

import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * Class Game
 */
public class Game {

    /**
     * Main method to start the game
     */
    public static void main(String[] args) {
        new Board();
    }
}
