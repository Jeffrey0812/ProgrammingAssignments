package board;

import card.Card;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.*;
import java.util.ArrayList;
import java.util.List;
import java.io.File;


/**
 * Class for Board
 */
public class Board extends JFrame {

    //Instance variables
    private List<Card> cardsList;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer time;
    private int tries = 0;

    private JFrame boardFrame;
    private JLabel labelTries;

    final static int pairs = 18; //Variable for number of pairs

    /**
     * Constructor
     */
    public Board() {


        List<Card> cardsList = new ArrayList<>(); //Arraylist for storing cards
        List<Integer> cardVals = new ArrayList<>(); //Arraylist for card Values

        /*Adding card values*/
        for (int i = 0; i < pairs; i++) {
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals); //Shuffling cards

        for (int value : cardVals) {
            String[] animals = {"alligator", "bird", "buffalo", "butterfly", "elephant", "fish", "gorilla", "kangaroo", "koala", "lion", "monkey", "panda", "penguin", "polar", "rhino", "snake", "tiger", "toucan"};
            try {
                /**Making a new card with pictures and their values*/
                File pathToFileCover = new File("C://Users//USER//Desktop//Semester 2//DDP-2//Lab//TP//assignment-4//src//main//java//game//img//back.png");
                File pathToFile = new File("C://Users//USER//Desktop//Semester 2//DDP-2//Lab//TP//assignment-4//src//main//java//game//img//" + animals[value] + ".png");
                Image cover = ImageIO.read(pathToFileCover);
                Image newCover = cover.getScaledInstance(140, 160, Image.SCALE_DEFAULT);
                Image img = ImageIO.read(pathToFile);
                Image newImage = img.getScaledInstance(140, 160, Image.SCALE_DEFAULT);
                Card c = new Card(value, new ImageIcon(newCover), new ImageIcon(newImage));
                c.getButton().addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        selectedCard = c;
                        doTurn(c);
                    }
                });
                cardsList.add(c);
            } catch (IOException e) {
                System.out.println("File not Found");
            }

        }
        this.cardsList = cardsList;

        time = new Timer(500, new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                checkCards();
            }
        });

        time.setRepeats(false);

        /**Making the layout of the board*/
        boardFrame = new JFrame("Match-Pair Memory Game");
        boardFrame.setLayout(new BorderLayout());
        JPanel boardGame = new JPanel();
        boardGame.setLayout(new GridLayout(6, 6));
        for (Card c : cardsList) {
            boardGame.add(c.getButton());
        }

        JPanel infoArea = new JPanel(new GridLayout(2, 2));
        JButton restartButton = new JButton("Restart");
        restartButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                restart(cardsList);
            }
        });
        infoArea.add(restartButton);

        JButton shuffleButton = new JButton("How to Play");
        shuffleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                help();
            }
        });
        infoArea.add(shuffleButton);

        labelTries = new JLabel("Number of Tries: " + tries);

        infoArea.add(labelTries);
        boardFrame.add(boardGame, BorderLayout.CENTER);
        boardFrame.add(infoArea, BorderLayout.SOUTH);
        boardFrame.setPreferredSize(new Dimension(1000, 1000));
        boardFrame.setLocation(500, 0);
        boardFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        boardFrame.pack();
        boardFrame.setVisible(true);
    }

    /**
     * Method to restart the game
     */
    private void restart(List<Card> cards) {
        for (Card c : cardsList) {
            c.setMatched(false);
            c.closeCard();
            c.getButton().setEnabled(true);
            c.getButton().setVisible(true);
        }
        tries = 0;
        labelTries.setText("Number of Tries: " + tries);
    }

    /**
     * Method to open help
     */
    private void help() {
        JOptionPane.showMessageDialog(null, "A game made by Javari Park. The game is a\n" +
                "match-pair memory game where the player will have to match animal characters\n" +
                "that are hidden behind 6x6 grid tiles. There are 18 animal characters hidden\n" +
                "in the game and player must find matching characters by checking the tiles.\n" +
                "When player found matching pair of characters, then the tiles that hid the\n" +
                "characters will disappear/be invisible.\n\n" +
                "Player can only open at most 2 tiles. If there are 2 open tiles and the\n" +
                "player wanted to open another tile, then previous 2 open tiles will be\n" +
                "closed. This game also tracks the number of attempts done by player to\n" +
                "match hidden character behind the tiles. Furthermore, this game also\n" +
                "lets player to restart the game from the beginning. Have fun!");
    }

    /**
     * Method to play
     */
    private void doTurn(Card selectedCard) {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.openCard();
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.openCard();
            time.start();
        }
    }

    /**
     * Method to check same cards
     */
    private void checkCards() {
        if (c1.getId() == c2.getId()) {
            c1.getButton().setEnabled(false);
            c1.getButton().setVisible(false);
            c2.getButton().setEnabled(false);
            c2.getButton().setVisible(false);
            c1.setMatched(true);
            c2.setMatched(true);
            tries += 1;
            labelTries.setText("Number of Tries: " + tries);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!\nNumber of tries: " + tries);
                System.exit(0);
            }
        } else {
            c1.closeCard();
            c2.closeCard();
            tries += 1;
            labelTries.setText("Number of Tries: " + tries);
        }
        c1 = null;
        c2 = null;
    }

    /**
     * Method to check if the game is won
     */
    private boolean isGameWon() {
        for (Card c : this.cardsList) {
            if (!c.getMatched()) {
                return false;
            }
        }
        return true;
    }
}
