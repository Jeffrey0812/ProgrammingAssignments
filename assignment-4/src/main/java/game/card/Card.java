package card;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import board.Board;

/**
 * Class for card
 */
public class Card {

    //Instance Variables
    private final int id;
    private final JButton button = new JButton();
    private boolean isMatched = false;
    private final ImageIcon pressedPic;
    private final ImageIcon icon;

    /**
     * Constructor
     */
    public Card(int id, ImageIcon icon, ImageIcon pressedPic) {
        this.id = id;
        this.icon = icon;
        this.pressedPic = pressedPic;
        this.button.setIcon(icon);
    }

    /**
     * Mutators and Accesors
     */
    public JButton getButton() {
        return button;
    }

    public void setMatched(boolean isMatched) {
        this.isMatched = isMatched;
    }

    public boolean getMatched() {
        return this.isMatched;
    }

    public int getId() {
        return this.id;
    }

    /**
     * Method to open card and close card
     */
    public void openCard() {
        button.setIcon(pressedPic);
    }

    public void closeCard() {
        button.setIcon(icon);
    }

}
