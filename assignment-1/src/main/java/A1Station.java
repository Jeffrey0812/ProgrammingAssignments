import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    /**
     * Membuat main method.
     */
    public static void main(String[] args) {
        int count = 0;

        Scanner input = new Scanner(System.in); //input

        int numberCats = Integer.parseInt(input.nextLine()); //meminta input untuk N

        //Membuat array
        WildCat[] cats = new WildCat[numberCats];
        TrainCar[] train = new TrainCar[numberCats];

        //Looping untuk menambahkan input sebanyak N dan menjalankan program seperti yang diminta
        for (int i = 0; i < numberCats; i++) {
            String[] info = input.nextLine().split(",");
            cats[i] = new WildCat(info[0],Double.parseDouble(info[1]),Double.parseDouble(info[2]));
            count += 1;
            if (count > 1) {
                train[i] = new TrainCar(cats[i], train[i - 1]);
            } else {
                train[i] = new TrainCar(cats[i]);
            }

            if (train[i].computeTotalWeight() >= THRESHOLD || i == numberCats - 1) {


                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<--");
                train[i].printCar();

                double averageMassIndex = train[i].computeTotalMassIndex() / (double) count;

                String category;
                if (averageMassIndex < 18.5) {
                    category = "underweight";
                } else if (averageMassIndex >= 18.5 && averageMassIndex < 25) {
                    category = "normal";
                } else if (averageMassIndex >= 25 && averageMassIndex < 30) {
                    category = "overweight";
                } else {
                    category = "obese";
                }

                System.out.println("Average mass index of all cats: " + averageMassIndex);
                System.out.println("In average, the cats in the train are " + category);

                count = 0;
            }
        }
    }
}
