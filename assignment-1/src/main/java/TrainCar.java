public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    // Instance variables
    WildCat cat;
    TrainCar next;

    /**
     * Constructor method yang menerima satu input.
     */
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    /**
     * Constructor method yang menerima dua input.
     */
    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    /**
     * Method untuk menghitung total berat.
     */
    public double computeTotalWeight() {
        double totalWeight;
        if (this.next == null) {
            totalWeight = this.cat.weight + EMPTY_WEIGHT;
            return totalWeight;
        } else {
            totalWeight = this.cat.weight + EMPTY_WEIGHT;
            return totalWeight + next.computeTotalWeight();
        }
    }

    /**
     * Method untuk menghitung total Mass Index.
     */
    public double computeTotalMassIndex() {
        double totalMassIndex;

        if (this.next == null) {
            totalMassIndex = cat.computeMassIndex();
            return totalMassIndex;
        } else {
            totalMassIndex = cat.computeMassIndex();
            return totalMassIndex + next.computeTotalMassIndex();
        }
    }

    /**
     * Membuat method untuk mencetak kucing yang ada di kereta.
     */
    public void printCar() {
        if (this.next != null) {
            System.out.print("(" + this.cat.name + ")");
            System.out.print("--");
            next.printCar();
        } else {
            System.out.println("(" + this.cat.name + ")");
        }
    }
}
