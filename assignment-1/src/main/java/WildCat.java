public class WildCat {

    //Instance variables
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    /** Membuat constructor method.*/
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    /** Membuat constructor method.*/
    public double computeMassIndex() {
        double bodyMassIndex = this.weight / (this.length * this.length / 10000);
        return bodyMassIndex;
    }
}
