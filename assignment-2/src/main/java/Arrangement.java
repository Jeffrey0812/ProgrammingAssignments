
import animal.Animal;
import animal.Cat;
import animal.Lion;
import animal.Eagle;
import animal.Parrot;
import animal.Hamster;

import java.util.ArrayList;

/**
 * Class arrangement untuk merorganisir
 */
public class Arrangement {

    //Instance variable
    private static final int TOTALLEVEL = 3;

    /*Method untuk memasukkan binatang ke level*/
    public static ArrayList<ArrayList<Animal>> arranger(Animal[] animal) {
        ArrayList<ArrayList<Animal>> result = new ArrayList<>();
        ArrayList<Animal> levelOne = new ArrayList<Animal>();
        ArrayList<Animal> levelTwo = new ArrayList<Animal>();
        ArrayList<Animal> levelThree = new ArrayList<Animal>();
        int length = animal.length;

        if (length >= 3) {
            int lengthCage1 = length / TOTALLEVEL;
            for (int i = 0; i < lengthCage1; i++) {
                levelOne.add(animal[i]);
            }

            int remainder = length - lengthCage1;
            int lengthCage2 = remainder / (TOTALLEVEL - 1);
            for (int x = lengthCage1; x < lengthCage2 + lengthCage1; x++) {
                levelTwo.add(animal[x]);
            }

            for (int y = lengthCage1 + lengthCage2; y < length; y++) {
                levelThree.add(animal[y]);
            }
        } else if (length == 2) {
            levelOne.add(animal[0]);
            levelTwo.add(animal[1]);
        } else {
            levelOne.add(animal[0]);
        }

        result.add(levelOne);
        result.add(levelTwo);
        result.add(levelThree);
        String location;

        if (animal[0] instanceof Lion || animal[0] instanceof Eagle) {
            location = "outdoor";
        } else {
            location = "indoor";
        }

        System.out.println("location: " + location);
        printOut(result);
        return result;
    }

    /*Method untuk me-arrange sesuai instruksi*/
    public static void rearrange(ArrayList<ArrayList<Animal>> oldList) {
        ArrayList<ArrayList<Animal>> newList = new ArrayList<ArrayList<Animal>>();
        ArrayList<Animal> newLevelOne = new ArrayList<Animal>();
        ArrayList<Animal> newLevelTwo = new ArrayList<Animal>();
        ArrayList<Animal> newLevelThree = new ArrayList<Animal>();
        for (int x = oldList.get(0).size() - 1; x >= 0; x--) {
            newLevelTwo.add(oldList.get(0).get(x));
        }
        for (int x = oldList.get(1).size() - 1; x >= 0; x--) {
            newLevelThree.add(oldList.get(1).get(x));
        }
        for (int x = oldList.get(2).size() - 1; x >= 0; x--) {
            newLevelOne.add(oldList.get(2).get(x));
        }
        newList.add(newLevelOne);
        newList.add(newLevelTwo);
        newList.add(newLevelThree);
        System.out.println("After rearrangement...");
        printOut(newList);
    }

    /**
     * Mencetak output
     */
    private static void printOut(ArrayList<ArrayList<Animal>> animalsList) {
        String cage = "";
        int length = 0;
        for (int x = 3; x > 0; x--) {
            System.out.print("level " + x + ": ");
            for (Animal one : animalsList.get(x - 1)) {
                System.out.print(one.getName());
                length = one.getBodyLength();
                if (one instanceof Lion) {
                    cage = ((Lion) one).getCageInfo().getSize();
                } else if (one instanceof Eagle) {
                    cage = ((Eagle) one).getCageInfo().getSize();
                } else if (one instanceof Parrot) {
                    cage = ((Parrot) one).getCageInfo().getSize();
                } else if (one instanceof Hamster) {
                    cage = ((Hamster) one).getCageInfo().getSize();
                } else if (one instanceof Cat) {
                    cage = ((Cat) one).getCageInfo().getSize();
                }
                System.out.print(" (" + length + " - " + cage + "), ");
            }
            System.out.println();
        }
        System.out.println();
    }


}
