package cage;

/**
 * Membuat class IndoorCage
 */
public class IndoorCage {

    //Instance variable
    private String size;

    /*Constructor*/
    public IndoorCage(int bodyLength) {

        if (bodyLength < 45) {
            this.size = "A";
        } else if (bodyLength >= 45 && bodyLength <= 60) {
            this.size = "B";
        } else {
            this.size = "C";
        }
    }

    /*Accessor*/
    public String getSize() {
        return size;
    }

}
