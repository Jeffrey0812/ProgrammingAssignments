package cage;

/**
 * Membuat class OutdoorCage
 */
public class OutdoorCage {

    //Instance variables
    private String size;

    public OutdoorCage(int bodyLength) {

        if (bodyLength < 75) {
            this.size = "A";
        } else if (bodyLength >= 75 && bodyLength <= 90) {
            this.size = "B";
        } else {
            this.size = "C";
        }
    }

    /*Accessor*/
    public String getSize() {
        return size;
    }

}
