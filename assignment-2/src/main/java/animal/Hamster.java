package animal;

import cage.IndoorCage;

/**
 * Membuat subclass dari Animal, yaitu Hamster
 */
public class Hamster extends Animal {

    //Instance Variables
    private static int counter = 0;
    private IndoorCage cageInfo;

    /*Constructor*/
    public Hamster(String name, int bodyLength, IndoorCage cageInfo) {
        super(name, bodyLength);
        this.cageInfo = cageInfo;

        counter += 1;
    }

    /*Accessor*/
    public IndoorCage getCageInfo() {
        return cageInfo;
    }

    public static int getCounter() {
        return counter;
    }

    /*Method gnaw*/
    public void gnaw() {
        System.out.println(getName() + " makes a voice: ngkkrit.. ngkkrrriiit");
    }

    /*Method run wheel*/
    public void runWheel() {
        System.out.println(getName() + " makes a voice: trrr…. trrr...");
    }
}
