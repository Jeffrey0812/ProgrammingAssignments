package animal;

import cage.IndoorCage;

import java.util.Random;
import java.util.ArrayList;

/**
 * Membuat subclass dari Animal, yaitu Cat
 */
public class Cat extends Animal {

    //Instance variables
    private IndoorCage cageInfo;
    private static int counter = 0;

    //Membuat variable untuk randomVoice yang akan digunakna di method cuddle
    String randomVoice = " ";

    /*Constructors*/
    public Cat(String name, int bodyLength, IndoorCage cageInfo) {
        super(name, bodyLength);
        this.cageInfo = cageInfo;

        counter += 1;
    }

    /*Accessors*/
    public IndoorCage getCageInfo() {
        return cageInfo;
    }

    public static int getCounter() {
        return counter;
    }

    /*Method brush the fur*/
    public void brushFur() {
        System.out.println("Time to clean " + getName() + "'s fur");
        System.out.println(getName() + " makes a voice: Nyaaan...");
    }

    /*Method cuddle*/
    public void cuddle() {
        ArrayList<String> catVoices = new ArrayList<String>();
        catVoices.add("Miaaaw..");
        catVoices.add("Purrr..");
        catVoices.add("Mwaw!");
        catVoices.add("Mraaawr!");
        randomVoice = catVoices.get(new Random().nextInt(catVoices.size()));
        System.out.println(getName() + " makes a voice " + randomVoice);
    }
}
