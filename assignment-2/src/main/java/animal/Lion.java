package animal;

import cage.OutdoorCage;

/**
 * Membuat subclass dari Animal, yaitu Lion
 */
public class Lion extends Animal {

    //Instance Variables
    private static int counter = 0;
    private OutdoorCage cageInfo;

    /*Constructor*/
    public Lion(String name, int bodyLength, OutdoorCage cageInfo) {
        super(name, bodyLength);
        this.cageInfo = cageInfo;

        counter += 1;
    }

    /*Accessors*/
    public OutdoorCage getCageInfo() {
        return cageInfo;
    }

    public static int getCounter() {
        return counter;
    }

    /*Method hunt*/
    public void hunt() {
        System.out.println("Lion is hunting..");
        System.out.println(getName() + " makes a voice: err...!");
    }

    /*Method brushMane*/
    public void brushMane() {
        System.out.println("Clean the lion's mane..");
        System.out.println(getName() + "  makes a voice: Hauhhmm!");
    }

    /*Method disturb*/
    public void disturb() {
        System.out.println(getName() + "  makes a voice: HAUHHMM");
    }
}
