package animal;

import java.util.Scanner;

import cage.IndoorCage;

/**
 * Membuat subclass dari Animal, yaitu Parrot
 */
public class Parrot extends Animal {

    //Inisiasi Input
    Scanner input = new Scanner(System.in);

    //Instance variables
    private static int counter = 0;
    private IndoorCage cageInfo;

    /*Constructor*/
    public Parrot(String name, int bodyLength, IndoorCage cageInfo) {
        super(name, bodyLength);
        this.cageInfo = cageInfo;

        counter += 1;
    }

    /*Accessor*/
    public IndoorCage getCageInfo() {
        return cageInfo;
    }

    public static int getCounter() {
        return counter;
    }

    /*Method fly*/
    public void fly() {
        System.out.println("Parrot " + getName() + " files!");
        System.out.println(getName() + " makes a voice: FLYYYY…..");
    }

    /*Method conversation*/
    public void conversation() {
        System.out.print("You say: ");
        String yourConversation = input.nextLine();
        System.out.println("\n" + getName() + " say: " + yourConversation.toUpperCase());
    }

    /*Method lonely*/
    public void lonely() {
        System.out.println((getName() + " says: HM?"));
    }
}
