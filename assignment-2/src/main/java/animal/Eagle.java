package animal;

import cage.OutdoorCage;

/**
 * Membuat subclass dari Animal, yaitu Eagle
 */
public class Eagle extends Animal {

    //Instance Variables
    private static int counter = 0;
    private OutdoorCage cageInfo;

    /*Constructor*/
    public Eagle(String name, int bodyLength, OutdoorCage cageInfo) {
        super(name, bodyLength);
        this.cageInfo = cageInfo;

        counter += 1;
    }

    /*Accessor*/
    public OutdoorCage getCageInfo() {
        return cageInfo;
    }

    public static int getCounter() {
        return counter;
    }

    /*Method fly*/
    public void fly() {
        System.out.println(getName() + " makes a voice: kwaakk…");
        System.out.println("You hurt!");
    }
}
