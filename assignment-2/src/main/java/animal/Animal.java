package animal;

/**
 * Membuat Superclass Animal
 */
public class Animal {

    //Instance variables
    private String name;
    private int bodyLength;

    /*Constructor*/
    public Animal(String name, int bodyLength) {
        this.name = name;
        this.bodyLength = bodyLength;
    }

    /*Accessors*/
    public String getName() {
        return name;
    }

    public int getBodyLength() {
        return bodyLength;
    }

}
