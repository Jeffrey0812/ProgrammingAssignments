import animal.Animal;
import animal.Cat;
import animal.Lion;
import animal.Eagle;
import animal.Hamster;
import animal.Parrot;
import cage.IndoorCage;
import cage.OutdoorCage;


import java.util.Scanner;

public class Main {

    //Instance variable
    private static String[] animalNames = {"Cat", "Lion", "Eagle", "Parrot", "Hamster"};

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        Animal[][] animalList = new Animal[5][];

        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");

        for (int i = 0; i < animalList.length; i++) {
            System.out.print(animalNames[i] + ": ");

            int noAnimal = input.nextInt();

            if (noAnimal != 0) {
                animalList[i] = new Animal[noAnimal];
                System.out.println("Provide the informations of " + animalNames[i] + "(s):");
                String animalsInfo = input.next();
                String[] animalsList = animalsInfo.split(",");
                for (int j = 0; j < animalsList.length; j++) {

                    String[] animalInfo = animalsList[j].split("\\|");
                    String name = animalInfo[0];
                    int bodyLength = Integer.parseInt(animalInfo[1]);

                    //Memasukkan binatang ke classnya
                    if (i == 0) {
                        animalList[i][j] = new Cat(name, bodyLength, new IndoorCage(bodyLength));
                    } else if (i == 1) {
                        animalList[i][j] = new Lion(name, bodyLength, new OutdoorCage(bodyLength));
                    } else if (i == 2) {
                        animalList[i][j] = new Eagle(name, bodyLength, new OutdoorCage(bodyLength));
                    } else if (i == 3) {
                        animalList[i][j] = new Parrot(name, bodyLength, new IndoorCage(bodyLength));
                    } else if (i == 4) {
                        animalList[i][j] = new Hamster(name, bodyLength, new IndoorCage(bodyLength));
                    }
                }
            }
        }

        System.out.println("Animals has been successfully recorded!");
        System.out.println("\n=============================================");
        System.out.println("Cage arrangement");

        for (Animal[] organism : animalList) {
            if (organism != null) {
                Arrangement.rearrange((Arrangement.arranger(organism)));
            }
        }

        //Mencetak jumlah binatang
        System.out.println("NUMBER OF ANIMALS: ");
        System.out.println("cat: " + Cat.getCounter());
        System.out.println("lion: " + Lion.getCounter());
        System.out.println("parrot: " + Parrot.getCounter());
        System.out.println("eagle: " + Eagle.getCounter());
        System.out.println("hamster: " + Hamster.getCounter());
        System.out.println("\n=============================================");

        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");

            int choices = 0;
            choices = input.nextInt();

            if (choices == 99) {
                System.out.println("Bye Bye");
                break;
            }

            String name = null;
            int index = 6;
            int amount = 0;
            switch (choices) {
                case 1:
                    name = "cat";
                    index = 0;
                    amount = Cat.getCounter();
                    break;

                case 2:
                    name = "eagle";
                    index = 2;
                    amount = Eagle.getCounter();
                    break;

                case 3:
                    name = "hamster";
                    index = 4;
                    amount = Hamster.getCounter();
                    break;

                case 4:
                    name = "parrot";
                    index = 3;
                    amount = Parrot.getCounter();
                    break;

                case 5:
                    name = "lion";
                    index = 1;
                    amount = Lion.getCounter();
                    break;

                default:
                    System.out.println("Animal with that number is not available");
                    break;
            }

            if (amount != 0) {
                System.out.print("Mention the name of " + name + " you want to visit: ");
                String animalName = input.next();
                boolean flag = false;
                Animal chosen = null;

                for (Animal one : animalList[index]) {
                    if (one.getName().equals(animalName)) {
                        flag = true;
                        chosen = one;
                        break;
                    }
                }

                if (flag) {
                    System.out.println("You are visiting " + chosen.getName() + " (" + name + ") now, what would you like to do?");
                    switch (index) {
                        case 0:
                            System.out.println("1: brush the fur 2: cuddle");
                            int choices0 = input.nextInt();
                            switch (choices0) {
                                case 1:
                                    ((Cat) chosen).brushFur();
                                    break;

                                case 2:
                                    ((Cat) chosen).cuddle();
                                    break;

                                default:
                                    System.out.println("You do nothing...");
                                    break;
                            }
                            break;

                        case 2:
                            System.out.println("1: Order to fly");
                            int choices1 = input.nextInt();
                            switch (choices1) {
                                case 1:
                                    ((Eagle) chosen).fly();
                                    break;

                                default:
                                    System.out.println("You do nothing...");
                                    break;
                            }
                            break;

                        case 4:
                            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
                            int choices2 = input.nextInt();
                            switch (choices2) {
                                case 1:
                                    ((Hamster) chosen).gnaw();
                                    break;

                                case 2:
                                    ((Hamster) chosen).runWheel();
                                    break;

                                default:
                                    System.out.println("You do nothing");
                                    break;
                            }
                            break;

                        case 3:
                            System.out.println("1: Order to fly 2: Do conversation");
                            int choices3 = input.nextInt();
                            switch (choices3) {
                                case 1:
                                    ((Parrot) chosen).fly();
                                    break;

                                case 2:
                                    ((Parrot) chosen).conversation();
                                    break;

                                default:
                                    ((Parrot) chosen).lonely();
                                    break;
                            }
                            break;

                        case 1:
                            System.out.println("1: See it hunting 2: brush the mane 3: disturb it");
                            int choices4 = input.nextInt();
                            switch (choices4) {
                                case 1:
                                    ((Lion) chosen).hunt();
                                    break;

                                case 2:
                                    ((Lion) chosen).brushMane();
                                    break;

                                case 3:
                                    ((Lion) chosen).disturb();
                                    break;

                                default:
                                    System.out.println("You do nothing");
                                    break;
                            }
                            break;

                        default:
                            System.out.println("You choose nothing");
                            break;
                    }
                    System.out.println("Back to the office! \n");
                } else {
                    System.out.println("There is no " + name + " with that name! Back to the office! \n");
                }
            } else if (choices < 0 || choices > 5) {
                System.out.println("Back to the office \n");
            } else {
                System.out.println("There are no " + name + " in this park");
            }
        }
    }
}
