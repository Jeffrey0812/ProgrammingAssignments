package animalChooser;

/**A class to print out Chooser in main method*/
public class SectionChooser {
  public static void SectionChooser() {
    System.out.println("\nJavari Park has 3 sections:\n
                        1. Explore the Mammals\n
                        2. World of Aves\n
                        3. Reptilian Kingdom");
    System.out.print("Please choose your preferred section (type the number): ");
  }
}
