package javari.animal.aves;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**Creating an abtract class for Mammals*/
public abstract class Mammals extends Animal {

  /**Instaance variable*/
  private boolean isPregnant;

  /**Constrcutor*/
  public class Mammals(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, String pregnant) {
    super(id, type, name, gender, length, weight, condition);

    if (pregnant.equals("pregnant")) {
      this.isPregnant = true;
    }
    else {
      this.isPregnant = false;
    }
  }

  /**Getter*/
  public boolean getPregancy() {
    return isPregnant;
  }
}
