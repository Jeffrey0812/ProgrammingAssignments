package javari.animal.mammals;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**A whale class that inherits from the superclass, Mammals*/
public class Whale extends Mammals {

  /**Constructor*/
  public Whale(Integer id, String name, Gender gender, double length, double weight, Condition condition, String pregnant) {
    super(id, name, gender, length, weigth, condition, pregnant);
  }

  /**A method to check the capability of the animal to perform*/
  protected boolean doAttraction() {
    if (!this.getisPregnant()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**A method to print output, which is to be used in the main method*/
  protected static void whaleChooser() {
    System.out.println("\n--Whale--\n
                      Attractions by Whale:\n
                      1. Circles of Fires\n
                      2. Counting Masters\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
  }
}
