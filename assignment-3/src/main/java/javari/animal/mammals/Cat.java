package javari.animal.mammals;

import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**Cat class that inherits from the superclass, Mammals*/
public class Cat extends Mammals {

  /**Constructor*/
  public Cat(Integer id, String name, Gender gender, double length, double weight, Condition condition, String pregnant) {
    super(id, name, gender, length, weigth, condition, pregnant);
  }

  /**Method to check the capability of an animal to do attraction*/
  protected boolean doAttraction() {
    if (!this.getisPregnant()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**Prints out these strings in the main method*/
  protected static void catChooser() {
    System.out.println("\n--Cat--\n
                        Attractions by Cat:\n
                        1. Dancing Animals\n
                        2. Passionate Coders\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
    }
}
