package javari.animal.mammals;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**A Hamster class that inherits from the superclass, Mammals*/
public class Hamster extends Mammals {

  /**Constructor*/
  public Hamster(Integer id, String name, Gender gender, double length, double weight, Condition condition, String pregnant) {
    super(id, name, gender, length, weigth, condition, pregnant);
  }

  /**Method to check the capability og the animal to perform*/
  protected boolean doAttraction() {
    if (!this.getisPregnant()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**A method used to print output which is to be used in the main method*/
  protected static void hamsterChooser() {
    System.out.println("\n--Hamster--\n
                      Attractions by Hamster:\n
                      1. Dancing Animals\n
                      2. Counting Masters\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
  }

}
