package javari.animal.mammals;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;
/**Class that inheits from Mammals*/
public class Lion extends Mammals {

  /**Constrcutor*/
  public Lion(Integer id, String name, Gender gender, double length, double weight, Condition condition, String pregnant) {
    super(id, name, gender, length, weigth, condition, pregnant);
  }

  /**Method to ehck the capability of an animal to perform*/
  protected boolean doAttraction() {
    if (gender.equals("male") && !this.getisPregnant()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**Print output in main method*/
  protected static void lionChooser() {
    System.out.println("\n--Lion--\n
                      Attractions by Lion:\n
                      1. Circles of Fires\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
  }
}
