package javari.animal.reptiles;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**A Snake Class that inherits Reptiles*/
public class Snakes extends Reptiles {

  /**Constrcuotr*/
  public Snakes(Integer id, String name, Gender gender, double length, double weight, Condition condition, String tame) {
    super(id, name, gender, length, weigth, condition, tame);
  }

  /**Cecking the capability of the animal to perform*/
  protected boolean doAttraction() {
    if (getisTamed()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**Printing output in main method*/
  protected static void snakeChooser() {
    System.out.println("\n--Snake--\n
                      Attractions by Snake:\n
                      1. Dancing Animals\n
                      2. Passionate Coders\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
  }
}
