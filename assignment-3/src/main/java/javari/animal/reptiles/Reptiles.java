package javari.animal.aves;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**Abstract class for Reptiles*/
public abstract class Reptiles extends Animal {

  /**Instance varaible*/
  private boolean isTamed;

  /**Constructor*/
  public class Reptiles(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, String tame) {
    super(id, type, name, gender, length, weight, condition);

    if (tame.equals("tame")) {
      this.isTamed = true;
    }
    else {
      this.isTamed = false;
    }
  }

  /**Getter*/
  public boolean getIsTamed() {
    return isTamed;
  }
}
