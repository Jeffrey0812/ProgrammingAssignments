package javari.animal.aves;
import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**A class that inherits from Aves*/
public class Eagle extends Aves {

  /**Constructor*/
  public Eagle(Integer id, String name, Gender gender, double length, double weight, Condition condition, String lay) {
    super(id, name, gender, length, weigth, condition, lay);
  }

  /**Method that is used to check the capability of an animal to perform*/
  protected boolean doAttraction() {
    if (!this.getLaidEggs()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**Method used to print in th emain Method*/
  protected static void eagleChooser() {
    System.out.println("\n--Eagle--\n
                      Attractions by Eagle:\n
                      1. Circles of Fires\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
  }
}
