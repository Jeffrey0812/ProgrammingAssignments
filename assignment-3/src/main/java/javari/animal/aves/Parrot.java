package javari.animal.aves;


import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**A subclass from Aves*/
public class Parrot extends Aves {

  /**Constructor*/
  public Parrot(Integer id, String name, Gender gender, double length, double weight, Condition condition, String lay) {
    super(id, name, gender, length, weigth, condition, lay);
  }

  /** Checking the capability of an animal to perform*/
  protected boolean doAttraction() {
    if (!this.getLaidEggs()) {
      return true;
    }
    else {
      return false;
    }
  }

  /**Printing these outputs in the main method*/
  protected static void parrotChooser() {
    System.out.println("\n--Parrot--\n
                      Attractions by Parrot:\n
                      1. Dancing Animals\n
                      2. Counting Masters\n");
    System.out.print("Please choose your preferred attractions (type the number): ");
  }
}
