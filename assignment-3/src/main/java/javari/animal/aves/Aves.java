package javari.animal.aves;

import javari.animal.Gender;
import javari.animal.Condition;
import javari.animal.Animal;

/**An Abtract class that extends the superclass of Animal*/
public abstract class Aves extends Animal {

  /**instance variable*/
  private boolean isLayingEggs;

  /**Constrcutor*/
  public class Aves(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, String lay) {
    super(id, type, name, gender, length, weight, condition);

    if (lay.equals("laying")) {
      this.isLayingEggs = true;
    }
    else {
      this.isLayingEggs = false;
    }
  }

  /**Getter*/
  public boolean getLaidEggs() {
    return isLayingEggs;
  }
}
