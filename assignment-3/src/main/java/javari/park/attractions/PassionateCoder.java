package javari.park.attractions;

import javari.animal.Animal;
import javari.park.SelectedAttraction;

import java.util.ArrayList;

/**Passionate coders class that implements SelectedAttraction*/
public class PassionateCoders implements SelectedAttraction {

  /**Instance variables*/
  private String name = "Passionate Coders";
  private ArrayList<Animal> performers = new ArrayList<Animal>();

  private String type;

  /**Constructor*/
    public PassionateCoders(String type) {
        this.type = type;
    }

    /**Setters and Getters*/
    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public ArrayList<Animal> getPerformers() {
        return this.performers;
    }

    /**Method to add performers*/
    public boolean addPerformer(Animal performer) {
        if (performer.canPerform()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
}

}
