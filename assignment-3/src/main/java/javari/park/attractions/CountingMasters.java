package javari.park.attractions;

import javari.animal.Animal;
import javari.park.SelectedAttraction;

import java.util.ArrayList;

/**Counting Masters class that implements SelectAttraction*/
public class CountingMasters implements SelectedAttraction {

  /**Instance variables*/
  private String name = "Counting Masters";
  private ArrayList<Animal> performers = new ArrayList<Animal>();
  private String type;

  /**Constructor*/
    public CountingMasters(String type) {
        this.type = type;
    }

    /**Stters and Getters*/
    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public ArrayList<Animal> getPerformers() {
        return this.performers;
    }

    /**Method to add peformers*/
    public boolean addPerformer(Animal performer) {
        if (performer.canPerform()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
}

}
