package javari.park.attractions;

import javari.animal.Animal;
import javari.park.SelectedAttraction;

import java.util.ArrayList;

/**A class that implements SelectedAttractions*/
public class DancingAnimals implements SelectedAttraction {

  /**Instance variables*/
  private String name = "Dancing Animals";
  private ArrayList<Animal> performers = new ArrayList<Animal>();
  private String type;

  /**Constrcutor*/
    public DancingAnimals(String type) {
        this.type = type;
    }

    /**Setter and Getter*/
    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public ArrayList<Animal> getPerformers() {
        return this.performers;
    }

    /**Add performers method*/
    public boolean addPerformer(Animal performer) {
        if (performer.canPerform()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
}

}
