package javari.park;

import java.util.ArrayList;
import java.util.List;

/**Visitor class that implements Registration*/
public class Visitors implements Registration {
    private String name;
    private static int idCount = 0;
    private int id;
    private List<SelectedAttraction> selectedAttraction = new ArrayList<>();

    /**Constructor*/
    public Visitors() {
        this.id = idCount;

        idCount += 1;
    }

    @Override
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    @Override
    public String getVisitorName() {
        return this.name;
    }

    @Override
    public int getRegistrationId() {
        return this.id;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.selectedAttraction;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.selectedAttraction.add(selected);
        return true;
    }
}
