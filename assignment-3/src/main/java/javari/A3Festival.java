package animal;
package park;
package reader;
package writer;
package animalChooser;

import javari.reader.*;
import java.io.IOException;
import java.nio.file.Paths;
import java.utils.Scanner;

import javari.animal.Animal;
import javari.park.Attraction;
import javari.park.Visitor;
import javari.reader.CsvReader;
import javari.writer.RegistrationWriter;

/**Main method Class*/
public class A3Festival {

  /**Iniating input and new object visitor*/
  static Scanner input = new Scanner(System.in);
  static Visitors visitor = new Visitors();

  /**Main Method*/
  public static void main(String[] args) {

    System.out.println("Welcome to Javari Park Festival - Registration Service!");
    System.out.print("... Opening default section database from data.");
    String defaultPath = "C:/Users/USER/Desktop/Semester 2/DDP-2/Lab/TP/assignment-3/data";

    /**Variables for reading lines*/
    String dataFromFile1;
    String dataFromFile2;
    String dataFromFile3;

    /**Variable for counting valid and invalid data*/
    int validCounter1 = 0;
    int invalidCounter1 = 0;
    int validCounter2 = 0;
    int invalidCounter2 = 0;
    int validCounter3 = 0;
    int invalidCounter3 = 0;

    /**CSV Reading*/
    try {
      CsvReader reader1 = new CsvReader(Paths.get(defaultPath + "/animals_categories.csv"));
      dataFromFile1 = reader.getLines();
      CsvReader reader2 = new CsvReader(Paths.get(defaultPath + "/animals_attractions.csv"));
      dataFromFile2 = reader.getLines();
      CsvReader reader3 = new CsvReader(Paths.get(defaultPath + "/animals_records.csv"));
      dataFromFile3 = reader.getLines();
    }
    catch (IOException e) {
      System.out.println("... File not found or incorrect file!");
      System.out.print("Please provide the source data path: ");
      defaultPath = input.nextLine();
    }

    System.out.println("\n... Loading... Success... System is populating data...");

    /**Selecting the correct data and coutning valid and invalid data*/
    for (String lines : dataFromFile1) {
      String[] data1 = lines.split(",");
      String type = data1[0];
      String category = data1[1];
      String section = data1[2];

      if(type.equals("Hamster") && category.equals("mammals") && section.equals("Explore the Mammals")) {
        validCounter1 + =1;

      }
      else if(type.equals("Lion") && category.equals("mammals") && section.equals("Explore the Mammals")) {
        validCounter1 += 1;
      }
      else if(type.equals("Cat") && category.equals("mammals") && section.equals("Explore the Mammals")) {
        validCounter1 += 1;
      }
      else if(type.equals("Eagle") && category.equals("aves") && section.equals("World of Aves")) {
        validCounter1 += 1;
      }
      else if(type.equals("Parrot") && category.equals("aves") && section.equals("World of Aves")) {
        validCounter1 += 1;
      }
      else if(type.equals("Snake") && category.equals("reptiles") && section.equals("Reptillian Kingdom")) {
        validCounter1 += 1;
      }
      else if(type.equals("Whale") && category.equals("mammals") && section.equals("Explore the Mammals")) {
        validCounter1 += 1;
      }
      else {
        invalidCounter1 += 1;
      }
    }

    for (String lines : dataFromFile2) {
      String[] data2 = lines.split(",");
      String type = data2[0];
      String attractions = data2[1];

      if (type.equals("Whale") && (attractions.equals("Circles of Fires") || attractions.equals("Counting Masters"))){
        validCounter2 += 1;
      }
      else if (type.equals("Lion") && attractions.equals("Circles of Fires")){
        validCounter2 += 1;
      }
      else if (type.equals("Eagle") && (attractions.equals("Circles of Fires")){
        validCounter2 += 1;
      }
      else if (type.equals("Cat") && (attractions.equals("Dancing Animals") || attractions.equals("Passionate Coders"))){
        validCounter2 += 1;
      }
      else if (type.equals("Snake") && (attractions.equals("Dancing Animals") || attractions.equals("Passionate Coders"))){
        validCounter2 += 1;
      }
      else if (type.equals("Parrot") && (attractions.equals("Dancing Animals") || attractions.equals("Counting Masters"))){
        validCounter2 += 1;
      }
      else if (type.equals("Hamster") && (attractions.equals("Dancing Animals") || attractions.equals("Counting Masters") || attractions.equals("Passionate Coders"))){
        validCounter2 += 1;
      }
      else {
        invalidCounter2 += 1;
      }
    }

    for (String lines : dataFromFile3) {
      String [] data3 = lines.split(",");
      int ID = Integer.parseInt(data3[0]);
      String animal = data3[1];
      String name = data3[2];
      String m/f = data3[3];
      Gender gender = Gender.parseGender(m/f);
      double bodyLengthCm = Double.parseDouble(data3[4]);
      double bodyWeightKg = Double.parseDouble(data3[5]);
      String specialStatus = data3[6];
      String condition = data3[7];
      Condition condition1 = Condition.parseCondition(condition);

      if(animal.equals("Hamster")) {
        Hamster hamster = new Hamster(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else if(animal.equals("Lion")) {
        Lion lion = new Lion(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else if(animal.equals("Cat")) {
        Cat cat = new Cat(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else if(animal.equals("Eagle")) {
        Eagle eagle = new Eagle(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else if(animal.equals("Parrot")) {
        Parrot parrot = new Parrot(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else if(animal.equals("Snake")) {
        Snake snake = new Snake(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else if(animal.equals("Whale")) {
        Whale whale = new Whale(ID, animal, name, gender, bodyLengthCm, bodyWeightKg, condition1, specialStatus);
        validCounter3 += 1;
      }
      else {
        invalidCounter3 += 1;
      }
    }

    System.out.println("Found " + validCounter1 + "valid sections and " + invalidCounter1 + " invalid sections");
    System.out.println("Found " + validCounter2 + " valid attractions and " +  invalidCounter2 + " invalid attractions");
    System.out.println("Found " + validCounter1 + " valid animal categories and " +  invalidCounter1 + " invalid animal categories");
    System.out.println("Found " + validCounter3 + " valid animal records and " +  invalidCounter3 + " invalid animal records");

  /**Choosing an animal, attraction, and many more, also moving it to json*/
  System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
  System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
  SectionChooser.SectionChooser();
  String choice = input.next();
  switch (choice) {
    case "1":
      MammalsChooser.MammalsChooser();
      String choice1 = input.next();
      switch(choice1) {
        case "1":
          Hamster.hamsterChooser();
          String choice2 = input.next();
          switch(choice2) {
            case "1":
              if (Hamster.doAttraction()) {
                System.out.println("Attraction(s) by " + animal);
                ArrayList<String> listAtraction = new ArrayList<>();
                animal = animal.toLowerCase();
                if (Park.getCircleOfFire().contains(animal)) {
            listAttraction.add("Circles Of Fires");
        }
        if (Park.getDancingAnimals().contains(animal)) {
            listAtraction.add("Dancing Animals");
        }
        if (Park.getCountingMasters().contains(animal)) {
            listAttraction.add("Counting Masters");
        }
        if (Park.getPassionateCoders().contains(animal)) {
            listAttraction.add("Passionate Coders");
        }

        int number = 1;
        for (String att: listAtraction) {
            System.out.println(number + ". " + att);
            number++;
        }

        System.out.println("Please choose your preferred attractions (type the number): ");
        String choiceString = input.nextLine();
        int choice = Integer.parseInt(choiceString);
        if (choiceString.equals("#")) {
            chooseAnimal(section);
        } else if (choice <= listAtraction.size() && choice > 0) {
            Attraction attraction = new Attraction(listAtraction.get(choice - 1),
                                                    animal);
            for (Animal hewan: Park.getListOfAnimal()) {
                if (hewan.getType().equalsIgnoreCase(animal)) {
                    attraction.addPerformer(hewan);
                }
            }
            inputNama(attraction);
        } else {
            System.out.println("Wrong input");
        }
    }


              }
            case "2":
              //Counting Masters
            case "#":
              MammalsChooser.MammalsChooser();
            default:
              System.out.println("Please choose a number");
          }

        case "2":
          Lion.lionChooser();
          String choice2 = input.next();
          switch(choice2) {
            case "1":
              //circle of fire
            case "#":
              MammalsChooser.MammalsChooser();
            default:
              System.out.println("Please choose a number");

        case "3":
          Cat.catChooser();
          String choice2 = input.next();
          switch(choice2) {
            case "1":
              //dancing anaimal
            case "2":
              //passionate coders
            case "#":
              MammalsChooser.MammalsChooser();
            default:
              System.out.println("Please choose a number");

        case "4":
          Whale.whaleChooser();
          String choice2 = input.next();
          switch(choice2) {
            case "1":
              //circle of fire
            case "2":
              //countng masters
            case "#":
              MammalsChooser.MammalsChooser();
            default:
              System.out.println("Please choose a number");


        case "#":
          SectionChooser.SectionChooser();

        default:
          System.out.println("Please choose a number");
      }
    case "2":

      String choice2 = input.next();
      switch(choice2) {
        case "1":
          //Eagle
        case "2":
          //parrot
        case "#":
          //
        default:
          System.out.println("Please choose a number");
      }
    case "3":

      String choice3 = input.next();
      switch(choice3) {
        case "1":

        default:
          System.out.println("Please choose a number");
        }
      }

    System.out.println("Wow, one more step,");
    System.out.print("please let us know your name: ");
    String name = input.nextLine();
    visitor.setVisitorName(name);

    System.out.println("\n\nYeay, final check!\n
                        Here is your data, and the attraction you chose:\n
                        Name: " + visitor.getVisitorName() + "\n" +
                        "Attractions: " + attraction.getName() + " -> " + attraction.getType());
    for (Animal one: attraction.getPerformers()) {
      perforemer.append(one.getName().append(", "));
    }
    System.out.println("With: " +performer.substring(0, performer.length() - 2));


    System.out.print("\nIs the data correct? (Y/N): ");
    char Y/N = input.next();

    if (Y/N.equals('Y')) {
      System.out.print("Thank you for your interest. Would you like to register to other attractions? (Y/N): ");
      char choice1 = input.next();
      if (choice1.equals('Y')){
        SectionChooser.SectionChooser();
      }
      else if (choice1.equals('N')) {
        try {
            RegistrationWriter.writeJson(visitor, Paths.get(defaultPath));
        } catch (IOException e) {
            System.out.println("\n... Destination folder not found\n");
            System.out.println("Please provide destination folder path: ");
            defaultPath = input.nextLine();
            printJson();
        }
      }
      else {
        System.out.println("Please type in Y/N")
      }
    }

    System.out.println("... End of program, write to " + registration_Dek_Depe + ".json ...");
  }
}
